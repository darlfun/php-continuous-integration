#!/bin/bash

for version in */ ; do
     tag=${version::-1}
     docker build -t $1:$tag $version
     docker push $1:$tag
done
